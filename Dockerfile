FROM python:3.9

WORKDIR /opt/app

RUN pip install --no-cache-dir virtualenv

RUN virtualenv /opt/v

COPY requirements.txt /opt/app/

RUN /opt/v/bin/pip install -r requirements.txt

COPY . /opt/app

EXPOSE 8000

CMD ["sh", "./startup.sh"]
