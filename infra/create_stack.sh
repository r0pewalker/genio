#! /bin/sh
aws --profile personal cloudformation create-stack --stack-name $1 --capabilities CAPABILITY__IAM --capabilities CAPABILITY_NAMED_IAM --template-body file://$PWD/$2
