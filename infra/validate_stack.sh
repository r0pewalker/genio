#! /bin/sh
# https://pypi.org/project/cfn-lint/
aws --profile personal cloudformation validate-template --template-body file://$PWD/$1 && cfn-lint $1
