from django.shortcuts import render

from .connectors import OmdbConnector
from .forms import SearchForm


# ENHANCE use redis for cache
#  a ratelimit makes little sense (OMDB is capped at 1k reqs/day)
def search(request):
    results = []
    form = SearchForm(initial=request.GET)
    connector = OmdbConnector(
        title=request.GET.get("title", ""),
        type=request.GET.get("type", ""),
        year=request.GET.get("year", ""),
    )
    results = connector.resolve()
    ret = {
        "form": form,
        "results": results
    }
    return render(request, 'omdb/search.html', ret)
