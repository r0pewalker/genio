from django import forms
from django.core.validators import RegexValidator


class SearchForm(forms.Form):
    title = forms.CharField(
        label='title',
        max_length=100,
        required=False
    )
    type = forms.ChoiceField(
        label='type',
        choices=(
            ("movie", "movie"),
            ("series", "series"),
            ("episode", "episode")
            ),
        required=False,
    )
    year = forms.CharField(
        label='year',
        max_length=4,
        validators=[RegexValidator(r'^\d{4}$')],
        required=False
    )
