from django.test import TestCase

from .connectors import OmdbConnector


# ENHANCE Selenium end-to-end tests would be a good fit
# since tested code is UX related
class OmdbConnectorTests(TestCase):

    def test_blank_search(self):
        """
        a search with no parameters returns no results
        """
        connector = OmdbConnector()
        results = connector.resolve()
        self.assertEqual(len(results), 0)

    def test_missing_required_param(self):
        """
        a search missing the required title param
        returns no results
        """
        # TODO make this check that the form wont validate
        connector = OmdbConnector(type="movie", year="1999")
        results = connector.resolve()
        self.assertEqual(len(results), 0)

    def test_typed_search(self):
        """
        a valid search for a specific type only returns
        results of that type
        """
        connector = OmdbConnector(type="movie", title="Blade")
        results = connector.resolve()
        self.assertNotEqual(len(results), 0)
        self.assertTrue(
            all([result.get("Type", "") == "movie" for result in results])
        )
