from django.shortcuts import render

from .connectors import AlphavantageConnector
from .forms import SymbolSearchForm


# ENHANCE use redis for cache
# a ratelimit makes little sense
# Alphavantage is capped at 5 reqs/minute and 500 reqs/day
def search_symbol(request):
    results = []
    form = SymbolSearchForm(initial=request.GET)
    connector = AlphavantageConnector(
        function="SYMBOL_SEARCH",
        symbol=request.GET.get("symbol", "")
    )
    results = connector.resolve()
    ret = {
        "form": form,
        "results": results
    }
    return render(request, 'alphavantage/symbol_search.html', ret)


# ENHANCE use redis for cache
# a ratelimit makes little sense
# Alphavantage is capped at 5 reqs/minute and 500 reqs/day
def symbol_detail(request, symbol):
    result = None
    connector = AlphavantageConnector(
        function="TIME_SERIES_INTRADAY",
        symbol=symbol,
        interval="60min"
    )
    result = connector.resolve()
    max_values = 10
    labels = list(reversed(result.keys()))[-max_values:]
    data = list(reversed(
        [float(v.get("4. close")) for v in result.values()]
    ))[-max_values:]
    ret = {
        "symbol": symbol,
        "labels": labels,
        "data": data
    }
    return render(request, 'alphavantage/symbol_detail.html', ret)
