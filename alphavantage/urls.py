from django.urls import path

from . import views

urlpatterns = [
    path('', views.search_symbol, name='search_symbol'),
    path('<str:symbol>/', views.symbol_detail, name='symbol_detail'),
]
