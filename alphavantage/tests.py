import datetime
import time

from django.test import TestCase

from .connectors import AlphavantageConnector


class AlphavantageConnectorTests(TestCase):

    def test_blank_symbol_search(self):
        """
        a symbol search missing the only parameter
        returns no results
        """
        connector = AlphavantageConnector(
            function="SYMBOL_SEARCH",
        )
        results = connector.resolve()
        self.assertEqual(len(results), 0)

    def test_basic_symbol_search(self):
        """
        a symbol search with a known valid query
        returns at least one result
        """
        connector = AlphavantageConnector(
            function="SYMBOL_SEARCH",
            symbol="Microsoft"
        )
        results = connector.resolve()
        self.assertNotEqual(len(results), 0)

    def test_accurate_symbol_search(self):
        """
        a symbol search with a known exact argument
        has its first result matching it
        """
        connector = AlphavantageConnector(
            function="SYMBOL_SEARCH",
            symbol="IBM"
        )
        results = connector.resolve()
        self.assertEqual(results[0].get("1. symbol"), "IBM")

    def test_incorrect_symbol_detail(self):
        """
        requesting detail about a symbol that does not exist
        returns no data
        """
        connector = AlphavantageConnector(
            function="TIME_SERIES_INTRADAY",
            symbol="GATTINI",
            interval="60min"
        )
        result = connector.resolve()
        self.assertEqual(result, {})

    def test_basic_symbol_detail(self):
        """
        requesting detail about an existing symbol
        returns a timeseries
        """
        connector = AlphavantageConnector(
            function="TIME_SERIES_INTRADAY",
            symbol="MSFT",
            interval="60min"
        )
        result = connector.resolve()
        self.assertNotEqual(len(list(result.keys())), 0)

    def test_symbol_detail_interval(self):
        """
        requesting detail about an existing symbol
        and a specific interval
        returns a timeseries on that interval
        """
        connector = AlphavantageConnector(
            function="TIME_SERIES_INTRADAY",
            symbol="AAPL",
            interval="60min"
        )
        # ENHANCE
        # AV's rate limiting would block tightly timed calls to this endpoint
        # this shoud be reflected in error handling and messages
        time.sleep(60)
        result = connector.resolve()
        self.assertNotEqual(result, {})
        datestrings = [k for k, v in result.items()][:2]
        dtformat = "%Y-%m-%d %H:%M:%S"
        parsed_datetimes = [
            datetime.datetime.strptime(datestring, dtformat)
            for datestring in datestrings
        ]
        delta = delta = (parsed_datetimes[0] - parsed_datetimes[1]).seconds
        self.assertEqual(delta, 3600)
