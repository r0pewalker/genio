from django import forms


class SymbolSearchForm(forms.Form):
    symbol = forms.CharField(
        label='symbol',
        max_length= 30,
        required=False
    )
