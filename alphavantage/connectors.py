import requests

from django.conf import settings

CONTENT_PROPERTIES = {
   "SYMBOL_SEARCH": {"key": "bestMatches", "default": []},
   "TIME_SERIES_INTRADAY": {"key": "Time Series (60min)", "default": {}}
}


class AlphavantageConnector():

    def __init__(self, **kwargs):
        self.function = kwargs.get("function")
        self.keywords = self.symbol = kwargs.get("symbol", "")
        self.interval = kwargs.get("interval", "")

    def resolve(self):
        payload = vars(self)
        payload.update({'apikey': settings.ALPHAVANTAGE_API_KEY})
        payload = {k: v for k, v in payload.items() if v}
        r = requests.get(
            settings.ALPHAVANTAGE_API_BASE_URL,
            params=payload
        )
        r.raise_for_status()
        return r.json().get(
            CONTENT_PROPERTIES[self.function]["key"],
            CONTENT_PROPERTIES[self.function]["default"]
        )
