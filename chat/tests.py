from django.test import TestCase
from channels.testing import WebsocketCommunicator
from chat.consumers import ChatConsumer


class ConsumerTests(TestCase):
    async def test_receive(self):
        communicator = WebsocketCommunicator(
            ChatConsumer.as_asgi(), "/ws/chat"
        )
        connected, subprotocol = await communicator.connect()
        assert connected
        await communicator.send_json_to({"message": "message"})
        response = await communicator.receive_json_from()
        assert response == {"type": "reply", "message": "message ack"}
        await communicator.disconnect()
