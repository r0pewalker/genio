# GENIO TEST

## DEV

```console
docker-compose up --build
```

## DEPLOY

### Requirements

- awscli & credentials
- .env

### Utils

The `infra` folder contains both cloudformation templates
and simple bash scripts to perform related operations

## APPLICATION UNITTESTS

run

```bash
./test.sh
```

## AUTOSCALING/LOAD TESTS

run

```bash
./infra/load_test.sh <ALB_URL>
```

check Cloudwatch metrics for CPU usage,
which triggers autoscaling verifiable in the ECS cluster console.
